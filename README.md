# stage-2-es6-for-everyone

## Instalation

`npm install`

`npm run build`

`npm run start`

open http://localhost:8080/

*Думаю, по хорошему, переменные которые хранят состояния бойца во время файта нужно хранить в отдельных для каждого бойца объектах,
у которых есть поля файтера и дополнительные переменные состояний. У меня же вместо этого один объект состояний. Проект делал постепенно 
и когда в обьекте было 4 переменные, это казалось нормальной идеей.*

##Play  Guide

1. Choose 2  fighters
2. **Fight!;)))**

**First fighters  controls:**
* Attack: press 'A'
* Block: press 'D'
* Critical damage (cool down 10  sec): press keys 'Q' + 'W' + 'E'

**Second fighters  controls:**
* Attack: press 'J'
* Block: press 'L'
* Critical damage (cool down 10  sec): press keys 'U' + 'I' + 'O'