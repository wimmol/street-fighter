import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  const setNewFields = (obj, side) => {
    obj.side = side;
    obj.isInBlock = false;
    obj.healthPer = 100;
    obj.isCritReady = true;
    obj.fullHealth = obj.health;
  };
  setNewFields(firstFighter, 'left');
  setNewFields(secondFighter, 'right');

  return new Promise((resolve) => {
    const critTimer = 10000;
    const critFactor = 2;

    document.addEventListener("keydown", logKeyDown);
    document.addEventListener("keyup", logKeyUp);

    const leftFighterIndicator = document.getElementById("left-fighter-indicator");
    const rightFighterIndicator = document.getElementById("right-fighter-indicator");

    let pressedKeys = new Set();

    function logKeyDown(key) {
      if (key.repeat){
        return;
      }
      pressedKeys.add(key.code);
      hit(key);
      healthBarView();
      endFight();
    }
    function logKeyUp(key) {
      pressedKeys.delete(key.code);

      switch (key.code)   {
        case controls.PlayerOneBlock:
          firstFighter.isInBlock = false;
          break;
        case controls.PlayerTwoBlock:
          secondFighter.isInBlock = false;
          break;
      }
    }

    function isCritReady(attacker, pos){
      let keys;
      let isReady;

      if (pos === 'left'){
        keys = controls.PlayerOneCriticalHitCombination;
        isReady = firstFighter.isCritReady;
      }
      else if (pos === 'right') {
        keys = controls.PlayerTwoCriticalHitCombination;
        isReady = secondFighter.isCritReady;
      }
      if (pressedKeys.has(keys[0]) &&
          pressedKeys.has(keys[1]) &&
          pressedKeys.has(keys[2]) &&
          isReady) {
        return true;
      }
      else {
        return false;
      }
    }

    function crit(attacker, pos){
      if (pos === 'left') {
        secondFighter.health -= attacker.attack * critFactor;
        firstFighter.isCritReady = false;
        setTimeout(() => { firstFighter.isCritReady = true }, critTimer);
      }
      else if (pos === 'right') {
        firstFighter.health -= attacker.attack * critFactor;
        secondFighter.isCritReady = false;
        setTimeout(() => { secondFighter.isCritReady = true }, critTimer);
      }
    }

    function healthBarView() {
      firstFighter.healthPer = firstFighter.health  * 100 / firstFighter.fullHealth;
      secondFighter.healthPer = secondFighter.health * 100 / secondFighter.fullHealth;
      if (firstFighter.healthPer < 0) firstFighter.healthPer = 0;
      if (secondFighter.healthPer < 0) secondFighter.healthPer = 0;
      leftFighterIndicator.style.width = `${firstFighter.healthPer }%`;
      rightFighterIndicator.style.width = `${secondFighter.healthPer}%`;
    }

    function endFight() {
      if (firstFighter.health <= 0) {
        document.removeEventListener("keydown", logKeyDown);
        document.removeEventListener("keyup", logKeyUp);
        resolve(secondFighter);
      }

      if (secondFighter.healthPer <= 0){
        document.removeEventListener("keydown", logKeyDown);
        document.removeEventListener("keyup", logKeyUp);
        resolve(firstFighter);
      }
    }

    function hit(key) {
      if (isCritReady(firstFighter, 'left')) {
        crit(firstFighter, 'left');
      }
      if (isCritReady(secondFighter, 'right')) {
        crit(secondFighter, 'right');
      }
      switch (key.code) {
        case controls.PlayerOneAttack:
          if (firstFighter.isInBlock) break;

          if (!secondFighter.isInBlock) {
            secondFighter.health -= getDamage(firstFighter, secondFighter);
          }
          break;
        case controls.PlayerTwoAttack:
          if (secondFighter.isInBlock) break;

          if (!firstFighter.isInBlock) {
            firstFighter.health -= getDamage(secondFighter, firstFighter);
          }
          break;
        case controls.PlayerOneBlock:
          firstFighter.isInBlock = true;
          break;
        case controls.PlayerTwoBlock:
          secondFighter.isInBlock = true;
          break;
      }
    }
  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  return fighter.defense * dodgeChance;
}
