import { createElement } from '../helpers/domHelper';


export function createFighterPreview(fighter, position) {
  if (typeof fighter  ==  "undefined"){
    fighter =  {
      "_id": "7",
      "name": "Choose second  hero",
      "health": "?",
      "attack": "?",
      "defense": "?",
      "source": "https://i.imgur.com/45HKXsE.gif"
    }
  }
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  const fighterImage  = createFighterImage(fighter);
  const fighterName = createElement('h2');
  const fighterHealth = createElement('p');
  const fighterAttack = createElement('p');
  const fighterDefense = createElement('p');

  fighterName.append(fighter.name);
  fighterHealth.append(`Health: ${fighter.health}`);
  fighterAttack.append(`Attack: ${fighter.attack}`);
  fighterDefense.append(`Defense: ${fighter.defense}`);
  fighterElement.append(fighterName, fighterImage, fighterHealth, fighterAttack, fighterDefense);

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
